#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import subprocess

import cgi

import datetime
from dateutil.relativedelta import relativedelta


import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../calendar/'))
import pan_calendar

 
form = cgi.FieldStorage()

this_file_dir=os.path.dirname(os.path.abspath(__file__))

calendar_form_html_file = this_file_dir + '/' + 'calendar_form.html'

def get_form_values ():
    form_vals = {'start': (form.getvalue('year_start'),
                           form.getvalue('month_start'),
                           form.getvalue('day_start')),
                 'goals': (form.getvalue('year_goals'),
                           form.getvalue('month_goals'),
                           form.getvalue('day_goals')),
                 'paper_dims': form.getvalue('paper_dims'),
                 'paper_place': form.getvalue('paper_place'),
                 }
    return form_vals


def check_form_inputs(form_vals):
    try:
        # datetime.datetime(2018, 2, 1, 12, 15, 30, 2000)
        start = (int(form_vals['start'][0]), int(form_vals['start'][1]), int(form_vals['start'][2]))
        dati_start = datetime.datetime(start[0], start[1], start[2])
        
        goals = (int(form_vals['goals'][0]), int(form_vals['goals'][1]), int(form_vals['goals'][2]))
        dati_goals = datetime.datetime(goals[0], goals[1], goals[2])
    except (ValueError, TypeError):
        dati_start = datetime.datetime.today().replace(day=1)
        dati_goals = dati_start + relativedelta(months=+6, days=-1) \
            # {6 month later and 1 days before} after dati_start.

    try:
        paper_dims, paper_place = form_vals['paper_dims'], form_vals['paper_place']
    except:
        paper_dims, paper_place = 'A4', 'landscape'
        
    return dati_start, dati_goals, (paper_dims, paper_place)



def remove_old_pdf_files(abs_pdfs_dir) :
    time_the_now = datetime.datetime.now()
    
    files = os.listdir(abs_pdfs_dir)
    remain_files = []
    
    for file in files:
        if file=='.gitkeep' :
            pass
        else:
            file = os.path.join(abs_pdfs_dir, file)
            time_file_creation = datetime.datetime.fromtimestamp(os.path.getctime(file))
            delta_time = (time_the_now - time_file_creation).total_seconds()

            if delta_time > 5 * 60 : # 5 minutes
                os.remove(file)
            else:
                remain_files.append(os.path.basename(file))

    return remain_files

if __name__ == '__main__':
    
    form_vals = get_form_values()
    
    dati_start, dati_goals, (paper_dims, paper_place) = check_form_inputs(form_vals)
    
    ip_name = os.environ['REMOTE_ADDR']
    current_date = datetime.datetime.now()

    filename_string = "calendar_" + ip_name + "_" + str(current_date) + ".pdf"

    if(os.environ['REQUEST_METHOD']=='POST'):
        #rel_pdf_path = "../tmp/" + filename_string
        #abs_pdf_path = os.path.join(os.path.dirname(__file__) ,rel_pdf_path)

        #tmp_files_directory = "//tmp/calendar-solar/"
        #href_pdf_path = "../tmp/" + filename_string
        #abs_pdf_path = os.path.join(tmp_files_directory + filename_string)

        tmp_files_directory = os.path.join(os.path.dirname(__file__) ,"../tmp/")
        href_pdf_path = "../tmp/" + filename_string
        abs_pdf_path = os.path.join(tmp_files_directory + filename_string)

        # temporay(tmp) directory check and create.
        if not os.path.exists(tmp_files_directory):
            os.makedirs(tmp_files_directory)

        # remove olds
        files = remove_old_pdf_files(os.path.dirname(abs_pdf_path))

        # generate calendar pdf
        pan_calendar.gen_calendar(dati_start, dati_goals,
                                  paper_size=paper_dims,
                                  landscape_or_portrait=paper_place,
                                  path=abs_pdf_path)

        # download link HTML
        download_pdf_tml = """Download: <a href="%s"> Download PDF </a>""" \
            % (href_pdf_path)

    elif(os.environ['REQUEST_METHOD']=='GET' or True):
        download_pdf_tml = """Download: Calendar is not generated yet."""


    with open(calendar_form_html_file, 'r') as file:
        html_text = file.read().replace('\n', '')
        
    form_dict = \
        {'year_start':dati_start.year, 'month_start':dati_start.month, 'day_start':dati_start.day,
         'year_goals':dati_goals.year, 'month_goals':dati_goals.month, 'day_goals':dati_goals.day,
         'messages': "", # form_vals, for test prints
         'download_pdf_tml': download_pdf_tml
         }
    
    html=html_text.format(**form_dict)

    
    print("Content-Type: text/html; charset=utf-8\n\n")
    print(html)
