
import datetime

def format_array(array):
    for row in array:
        for val in row:
            val = '_' if val == None else val
            print("{:<5}".format(val), end='')
        print()

def calendar_array():
    return [None] * (7+31-1 +1+1)

def make_calender(day_from, day_to):
    
    ## week line
    week_line = calendar_array()
    #week_text = "SMTWTFS"
    week_text = "ABCDEFG"

    #weekday_headday = day_from.isoweekday()
    #print(weekday_headday)
    
    for i in range(len(week_line)):
        #week_line[i] = None if (i<2) else (i+weekday_headday)%7 # week_text [(i-1-1) % 7]
        week_line[i] = None if (i<2) else (i-1-1)%7 # week_text [(i-1-1) % 7]
        
    ## year month and days lines

    day_this = day_from
    
    month_counter = -1
    week_split = 0

    months_array = []
    tomorrow_month = 0
    
    while(True):
        if(tomorrow_month != day_this.month):
            month_counter += 1
            months_array.append(calendar_array())
            months_array[month_counter][1] = day_this.month
            months_array[month_counter][0] = day_this.year

            # moluder 0 = from Monday to Sunday
            week_split = (day_this.weekday()+1)%7
            
        months_array[month_counter][day_this.day-1 +  1+1 + week_split] = day_this.day
        tomorrow_month = day_this.month
        
        day_this = day_this + datetime.timedelta(days=1)

        if(day_this > day_to):
            break

    ## append

    months_array = [week_line] + months_array
    
    return months_array
