import numpy as np
import matplotlib.pyplot as plt
from matplotlib import transforms
from pyrocko.plot import mpl_init, mpl_margins, mpl_papersize

from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.font_manager import FontProperties

import distro

# constants


def is_years_line(i,j):  return j == 0
def is_months_line(i,j): return j == 1
def is_weeks_line(i,j):  return i == 0
def is_metadata_lines(i,j):
    return is_years_line(i,j) or is_months_line(i,j) or is_weeks_line(i,j)
def is_day_cell(i,j): return not is_metadata_lines(i,j)


# font config by OS

distro_id = distro.id()
if distro_id == 'arch':
    font_path1 = r'/usr/share/fonts/TTF/Ricty-Regular.ttf'
    fp_ric = FontProperties(fname=font_path1)

    font_path2 = r'/usr/share/fonts/OTF/ipagp.ttf'
    fp_ipa = FontProperties(fname=font_path2)
elif distro_id == 'centos':
    font_path1 = r'/usr/share/fonts/ipa-gothic/ipag.ttf'
    fp_ric = FontProperties(fname=font_path1)

    font_path2 = r'/usr/share/fonts/ipa-gothic/ipag.ttf'
    fp_ipa = FontProperties(fname=font_path2)



# functions

def gen_papersize(paper_size, landscape_or_portrait):
    a4paper_portrait = (8.27, 11.69) # inch
    scale = \
        1/2 if paper_size == 'A5' else \
        1 if paper_size == 'A4' else \
        2 if paper_size == 'A3' else \
        4 if paper_size == 'A2' else \
        1

    paper_size = (scale * a4paper_portrait[0], scale * a4paper_portrait[1])

    paper = paper_size
    if landscape_or_portrait == "landscape" :
        paper = (paper_size[1], paper_size[0])
    elif landscape_or_portrait == "portrait" :
        paper = (paper_size[0], paper_size[1])

    return paper


def plot_cell_box(plt, y, x, ygap, xgap):
    plt.hlines(y+ygap, (x+xgap), (x-xgap), color='black', linewidth=0.4)
    plt.hlines(y-ygap, (x+xgap), (x-xgap), color='black', linewidth=0.4)
    plt.vlines(x+xgap, (y+ygap), (y-ygap), color='black', linewidth=0.4)
    plt.vlines(x-xgap, (y+ygap), (y-ygap), color='black', linewidth=0.4)


def text_cell_box(i, j, months_array):
    tmp = months_array[i][j]
    text = (
        "年" if is_weeks_line(i,j) and is_years_line(i,j) else
        "月" if is_weeks_line(i,j) and is_months_line(i,j) else
        "日月火水木金土"[tmp % 7] if is_weeks_line(i,j) else
        "" if is_years_line(i,j) and tmp == months_array[i-1][j] else
        tmp)
    return text


def plot_months_array(months_array,
                      paper_size='A4', landscape_or_portrait='landscape',
                      margins=(1.00,1.00,1.00,1.00) # (left, top, right, bottom) unit is cm
                      ):
    
    ## pdf figure
    height = len(months_array)
    width  = len(months_array[0])

    fig = plt.figure(figsize=gen_papersize(paper_size, landscape_or_portrait))

    
    mpl_margins(fig, units='cm',
                left=margins[0], top=margins[1], right=margins[2], bottom=margins[3])
    

    padding_x = 0.0
    padding_y = 0.0
    plt.xlim([0-0.14 -padding_y, height-0.5 +padding_y])
    plt.ylim([0-1/2  -padding_x, width-1/2  +padding_x])
    
    #labelpos=mpl_margins(plt, )

    # first of all, the base transformation of the data points is needed
    base = plt.gca().transData
    rot = transforms.Affine2D().rotate_deg(90)
    
    for i in range(len(months_array)):
        for j in range(len(months_array[i])):
            
            hgap=0.50
            wgap_fst = 3/20
            
            if is_weeks_line(i,j):
                wgap = wgap_fst
                dj, di = j, i
            else: # fill space by line
                wgap = 1/2 + 1 / (2*(height-1)) * (1/2-wgap_fst)
                dj, di = j, i - (1/2-wgap_fst) * ((height-1)-(i-1/2))/(height-1)

            plot_cell_box(plt, dj, di, hgap, wgap)
            
            if is_months_line(i,j) and i==0 and is_weeks_line:
                h_space = 0
                w_space = 0
                plt.hlines(dj+hgap+h_space, height-1/2, (0-wgap), color='black', linewidth=0.8)
                plt.vlines(di+wgap+w_space, width-1/2, (0-hgap), color='black', linewidth=0.8)

            if is_day_cell and months_array[i][j] == None:
                pass

            text = text_cell_box(i, j, months_array)
            
            if is_metadata_lines(i,j):
                pos_i = di
                h_a='center'
                font_size = 10.5
            elif is_day_cell(i,j):
                pos_i = di-hgap
                h_a='left'
                font_size = 8

            fp = fp_ric if type(text)==int else fp_ipa
            
            plt.text(pos_i, dj, text, # "%s %d %d" % (text, i, j),
                     ha=h_a, va='center',
                     fontproperties=fp,
                     color="black",
                     fontsize=font_size, clip_on=True)

    plt.gca().invert_yaxis()

    #plt.axis('off')
    plt.yticks([])
    plt.xticks([])
    
    #plt.show()

    return plt, fig
